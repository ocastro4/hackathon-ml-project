package com.example.mlexample;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FraudDetectionController {

    @PostMapping("/predict")
    public PredictionResponse predict(@RequestBody PredictionRequest request) {
        // Perform your prediction logic here
        // For demonstration purposes, let's assume the prediction is always 1
        return new PredictionResponse(1);
    }

}
