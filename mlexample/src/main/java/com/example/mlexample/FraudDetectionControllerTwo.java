package com.example.mlexample;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;

@RestController
public class FraudDetectionControllerTwo {

    @Value("${sagemaker.endpoint}")
    private String sagemakerEndpoint;

    @PostMapping("/two")
    public PredictionResponse predict(@RequestBody PredictionRequest request) {
        // Call SageMaker endpoint with the dataset
        RestTemplate restTemplate = new RestTemplate();
        PredictionResponse response = restTemplate.postForObject(sagemakerEndpoint, request.getData(), PredictionResponse.class);
        return response;
    }

}
