package com.example.mlexample;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class MlexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MlexampleApplication.class, args);
	}


	public class ServerlessController implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

		@Value("${sagemaker.endpoint}")
		private String sagemakerEndpoint;

		@Override
		public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
			String requestBody = input.getBody();

			// Call SageMaker endpoint with the input data
			RestTemplate restTemplate = new RestTemplate();
			String response = restTemplate.postForObject(sagemakerEndpoint, requestBody, String.class);

			// Create response for API Gateway
			APIGatewayProxyResponseEvent apiResponse = new APIGatewayProxyResponseEvent();
			apiResponse.setStatusCode(200);
			apiResponse.setBody(response);

			return apiResponse;
		}
	}

}
