package com.example.mlexample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PredictionResponse {
    private int prediction;

    public PredictionResponse() {
    }

    public PredictionResponse(@JsonProperty("prediction") int prediction) {
        this.prediction = prediction;
    }

    public int getPrediction() {
        return prediction;
    }

    public void setPrediction(int prediction) {
        this.prediction = prediction;
    }
}
